package com.zenika.poei.evaluationfinale.controller.Dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.zenika.poei.evaluationfinale.domain.Project;
import com.zenika.poei.evaluationfinale.domain.Ticket;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ProjectDto {
    public static ProjectDto fromDomain(Project project){
        ProjectDto dto = new ProjectDto();
        dto.setId(project.getId());
        dto.setName(project.getName());
        dto.setDescription(project.getDescription());
        dto.setCreatedAt(project.getCreatedAt());
        dto.setTickets(project.getTickets());
        return dto;
    }

    private Integer id;
    private String name;
    private String description;
    private ZonedDateTime createdAt;

    private List<Ticket> tickets = new ArrayList<>();


    public void setId(Integer id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public Integer getId() {
        return id;
    }
    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ZonedDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(ZonedDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public List<Ticket> getTickets() {
        return tickets;
    }

    public void setTickets(List<Ticket> tickets) {
        this.tickets = tickets;
    }



}
