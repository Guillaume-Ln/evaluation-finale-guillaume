package com.zenika.poei.evaluationfinale.controller;

import com.zenika.poei.evaluationfinale.controller.Dto.ProjectDto;
import com.zenika.poei.evaluationfinale.controller.Dto.ProjectInputDto;
import com.zenika.poei.evaluationfinale.controller.Dto.TicketDto;
import com.zenika.poei.evaluationfinale.controller.Dto.TicketInputDto;
import com.zenika.poei.evaluationfinale.domain.Project;
import com.zenika.poei.evaluationfinale.domain.Ticket;
import com.zenika.poei.evaluationfinale.service.ProjectService;
import io.swagger.v3.oas.annotations.security.SecurityRequirements;
import jakarta.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.List;

import static java.util.stream.Collectors.toList;


@RestController
@RequestMapping("api/zira")
@SecurityRequirements
public class ProjectController {

    private final ProjectService projectService;

    public ProjectController(ProjectService projectService) {
        this.projectService = projectService;
    }

    @GetMapping
    public ResponseEntity<List<ProjectDto>> getAllProjects(){
        List<ProjectDto> projects = projectService.listProjects().stream().map(ProjectDto::fromDomain).collect(toList());


        return ResponseEntity.ok(projects);
    }

    @GetMapping("/{id}")
    public ResponseEntity<ProjectDto> getProjectById(@PathVariable("id") Integer projectId){
        Project project = projectService.getProject(projectId);

        return ResponseEntity.ok(ProjectDto.fromDomain(project));
    }

    @GetMapping("/{id}/ticket/{ticketId}")
    public ResponseEntity<TicketDto> getTicketById(@PathVariable("id") Integer projectId, @PathVariable("ticketId")Integer ticketId) {
        Project project = projectService.getProject(projectId);
        Ticket ticket = projectService.getTicket(ticketId);

        return ResponseEntity.ok(TicketDto.fromDomain(ticket));
    }



    @PostMapping
    public ResponseEntity<ProjectDto> createProject(@RequestBody @Valid ProjectInputDto dto) {
        Project createdProject = projectService.createProject(dto.toDomain());

        //return ResponseEntity.created(URI.create("/zira/" + createdProject.getId()))
           //     .body(ProjectDto.fromDomain(createdProject));
        return ResponseEntity.ok().body(ProjectDto.fromDomain(createdProject));
    }



    @PostMapping("/{id}/tickets")
    public ResponseEntity<TicketDto> addTicket(@PathVariable("id") Integer projectId, @RequestBody @Valid TicketInputDto dto) {
        Ticket ticket = projectService.addTicket(projectId, dto.toDomain());
        return ResponseEntity.ok(TicketDto.fromDomain(ticket));


    }

    @PutMapping("/{id}")
    public ResponseEntity<ProjectDto> updateProject(@PathVariable("id") Integer projectId, @RequestBody @Valid ProjectInputDto dto) {
        Project updatedProject = projectService.updateProject(projectId, dto.toDomain());

        return ResponseEntity.ok(ProjectDto.fromDomain(updatedProject));
    }

    @PutMapping("/{id}/ticket/{ticketId}")
    public ResponseEntity<TicketDto> updateTicket(@PathVariable("id") Integer projectId,@PathVariable("ticketId")Integer ticketId, @RequestBody @Valid TicketInputDto dto) {
        Project project = projectService.getProject(projectId);
        Ticket updatedTicket = projectService.updateTicket(ticketId, dto.toDomain());

        return ResponseEntity.ok(TicketDto.fromDomain(updatedTicket));
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteProject(@PathVariable("id") Integer projectId){
        projectService.deleteProject(projectId);
    }

    @DeleteMapping("/{id}/ticket/{ticketId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteTicket(@PathVariable("id") Integer projectId,@PathVariable("ticketId")Integer ticketId, @RequestBody @Valid TicketInputDto dto) {
        Project project = projectService.getProject(projectId);
        projectService.deleteTicket(ticketId);
    }

}
