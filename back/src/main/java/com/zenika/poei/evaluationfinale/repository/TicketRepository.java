package com.zenika.poei.evaluationfinale.repository;

import com.zenika.poei.evaluationfinale.domain.Ticket;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TicketRepository extends JpaRepository<Ticket, Integer> {
}
