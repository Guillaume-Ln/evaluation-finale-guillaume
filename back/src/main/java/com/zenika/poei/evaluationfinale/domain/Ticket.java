package com.zenika.poei.evaluationfinale.domain;


import com.zenika.poei.evaluationfinale.controller.Dto.TicketStatus;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.DynamicInsert;

import java.time.ZonedDateTime;


@NoArgsConstructor
@Getter
@Setter
@Table(name = "tickets")
@DynamicInsert
@Entity
public class Ticket {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String title;
    private String content;

    private ZonedDateTime createdAt;

    @Enumerated(EnumType.STRING)
    private TicketStatus status;

    private Integer project_id;

    public void setStatus(TicketStatus status) {
        this.status = status;
    }
}
