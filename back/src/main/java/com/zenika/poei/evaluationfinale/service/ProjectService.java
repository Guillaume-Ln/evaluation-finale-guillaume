package com.zenika.poei.evaluationfinale.service;

import com.zenika.poei.evaluationfinale.domain.Project;
import com.zenika.poei.evaluationfinale.domain.Ticket;
import com.zenika.poei.evaluationfinale.repository.ProjectRepository;
import com.zenika.poei.evaluationfinale.repository.TicketRepository;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProjectService {

    private final ProjectRepository projectRepository;

    private final TicketRepository ticketRepository;

    public ProjectService(ProjectRepository projectRepository, TicketRepository ticketRepository) {
        this.projectRepository = projectRepository;
        this.ticketRepository = ticketRepository;
    }

    /** @Get endpoint*/
    public List<Project> listProjects() {
        return projectRepository.findAll(Sort.by(Sort.Direction.ASC,"createdAt"));
    }

    /** @Get endpoint By Id*/
    public Project getProject(Integer projectId) {
        return projectRepository.findById(projectId).orElseThrow();
    }

    public Ticket getTicket(Integer ticketId) {
        return ticketRepository.findById(ticketId).orElseThrow();
    }

    /**@Post endpoint*/
    public Project createProject(Project project) {
        return projectRepository.save(project);
    }
    public Ticket addTicket(Integer projectId, Ticket ticket) {
        ticket.setProject_id(projectId);
        Ticket createdTicket = ticketRepository.save(ticket);
        return createdTicket;

    }

    /**@Put endpoint to update project*/
    public Project updateProject(Integer projectId, Project updatedProject) {
        Project existingProject = projectRepository.findById(projectId).orElseThrow();
        existingProject.setName(updatedProject.getName());
        existingProject.setDescription(updatedProject.getDescription());

        return projectRepository.save(existingProject);


    }


    public Ticket updateTicket(Integer ticketId, Ticket updatedTicket) {

        Ticket existingTicket = ticketRepository.findById(ticketId).orElseThrow();
        existingTicket.setTitle(updatedTicket.getTitle());
        existingTicket.setContent(updatedTicket.getContent());
        existingTicket.setStatus(updatedTicket.getStatus());

        return ticketRepository.save(existingTicket);
    }

    public void deleteProject(Integer projectId){
        Project project = projectRepository.findById(projectId).orElseThrow();
        projectRepository.delete(project);
    }

    public void deleteTicket(Integer ticketId) {
        Ticket ticket = ticketRepository.findById(ticketId).orElseThrow();
        ticketRepository.delete(ticket);
    }
}
