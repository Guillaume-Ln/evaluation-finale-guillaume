package com.zenika.poei.evaluationfinale.controller.Dto;

import com.zenika.poei.evaluationfinale.domain.Project;
import jakarta.validation.constraints.NotBlank;

public class ProjectInputDto {

    @NotBlank
    private String name;

    @NotBlank
    private String description;

    public String getName() {
        return name;
    }



    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Project toDomain(){
        Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        return project;
    }
}
