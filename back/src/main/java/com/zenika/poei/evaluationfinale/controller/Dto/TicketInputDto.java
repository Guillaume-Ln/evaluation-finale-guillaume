package com.zenika.poei.evaluationfinale.controller.Dto;

import com.zenika.poei.evaluationfinale.domain.Ticket;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;

public class TicketInputDto {


    @NotNull
    private String title;

    @NotNull
    private String content;


    private TicketStatus status;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public TicketStatus getStatus() {
        return status;
    }

    public void setStatus(TicketStatus status) {
        this.status = status;
    }

    public Ticket toDomain(){
        Ticket ticket = new Ticket();
        ticket.setTitle(title);
        ticket.setContent(content);
        ticket.setStatus(status);
        return ticket;
    }
}
