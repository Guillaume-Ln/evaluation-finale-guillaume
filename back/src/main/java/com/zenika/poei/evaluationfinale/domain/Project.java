package com.zenika.poei.evaluationfinale.domain;


import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.DynamicInsert;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

@Entity
@NoArgsConstructor
@Getter
@Setter
@Table(name = "projects")
@DynamicInsert
public class Project {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String name;
    private String description;
    private ZonedDateTime createdAt;

    /*
    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "tickets", joinColumns = @JoinColumn(name = "project_id"))
     */

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true,fetch = FetchType.EAGER)
    @JoinColumn(name = "project_id")
    private List<Ticket> tickets = new ArrayList<>();



    public List<Ticket> getTickets() {
        return List.copyOf(tickets);
    }

    public void setTickets(List<Ticket> tickets) {
        this.tickets = tickets;
    }

    public void addTicket(Ticket ticket) {
        ticket.setCreatedAt(ZonedDateTime.now());
        tickets.add(ticket);
    }
}
