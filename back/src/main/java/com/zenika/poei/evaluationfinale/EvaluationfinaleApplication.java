package com.zenika.poei.evaluationfinale;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EvaluationfinaleApplication {

	public static void main(String[] args) {
		SpringApplication.run(EvaluationfinaleApplication.class, args);
	}

}
