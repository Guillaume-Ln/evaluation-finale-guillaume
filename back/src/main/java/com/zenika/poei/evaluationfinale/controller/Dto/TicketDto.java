package com.zenika.poei.evaluationfinale.controller.Dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.zenika.poei.evaluationfinale.domain.Ticket;

import java.time.ZonedDateTime;


@JsonInclude(JsonInclude.Include.NON_NULL)
public class TicketDto {

    public TicketDto() {
    }

    public static TicketDto fromDomain(Ticket ticket){
        TicketDto dto = new TicketDto();
        dto.setId(ticket.getId());
        dto.setTitle(ticket.getTitle());
        dto.setContent(ticket.getContent());
        dto.setCreatedAt(ticket.getCreatedAt());
        dto.setStatus(ticket.getStatus());

        return dto;
    }
    private Integer id;
    private String title;
    private String content;
    private ZonedDateTime createdAt;
    private TicketStatus status;
    private Integer project_id;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public ZonedDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(ZonedDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public TicketStatus getStatus() {
        return status;
    }

    public void setStatus(TicketStatus status) {
        this.status = status;
    }
}
