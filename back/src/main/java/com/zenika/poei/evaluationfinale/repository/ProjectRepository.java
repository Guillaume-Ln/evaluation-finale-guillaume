package com.zenika.poei.evaluationfinale.repository;

import com.zenika.poei.evaluationfinale.domain.Project;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProjectRepository extends JpaRepository<Project, Integer>{

}
