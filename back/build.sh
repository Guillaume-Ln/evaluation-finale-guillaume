#!/usr/bin/env bash

set -e

#based_tag_name=${CI_REGISTRY_IMAGE:-guillaume.lanoe/evaluation-finale-guillaume}


mvn clean package
docker build -t "evaluation-finale/back:latest" .