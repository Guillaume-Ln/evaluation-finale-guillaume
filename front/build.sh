#!/bin/bash

set -e

based_tag_name=${CI_REGISTRY_IMAGE:-guillaume.lanoe/evaluation-finale-guillaume}


npm install
#npm run test
npm run build
docker build -t "$based_tag_name/front:latest" .
