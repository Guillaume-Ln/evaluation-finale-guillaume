import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { icons, LucideAngularModule } from 'lucide-angular';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './component/layout/header/header.component';
import { ProjectsComponent } from './component/pages/projects/projects.component';
import { HttpClientModule } from '@angular/common/http';
import { ProjectsDetailsComponent } from './component/pages/projects-details/projects-details.component';
import { TicketCardComponent } from './ticket-card/ticket-card.component';
import { NewProjectComponent } from './component/pages/new-project/new-project.component';
import { ReactiveFormsModule } from '@angular/forms';
import { FieldModule } from "./component/ui/field/field.module";
import { NewTicketComponent } from './component/pages/new-ticket/new-ticket.component';

@NgModule({
    declarations: [
        AppComponent, HeaderComponent, ProjectsComponent, ProjectsDetailsComponent, TicketCardComponent, NewProjectComponent, NewTicketComponent
    ],
    providers: [],
    bootstrap: [AppComponent],
    imports: [
        BrowserModule,
        AppRoutingModule,
        HttpClientModule,
        LucideAngularModule,
        ReactiveFormsModule,
        FieldModule
    ]
})
export class AppModule { }
