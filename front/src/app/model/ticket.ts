import { TicketStatus } from "./ticket-status";

export class Ticket {
    id:number;
    title: string;
    content: string;
    createdAt: string;
    status: TicketStatus;
    project_id : number;



    constructor(    id:number,
        title: string,
        content: string,
        createdAt: string,
        status: TicketStatus,
        project_id : number){
            this.id=id;
            this.title=title;
            this.content=content;
            this.createdAt=createdAt;
            this.status=status;
            this.project_id = project_id;
        }
}
