import { Ticket } from "./ticket";

export class Project {
    id:number;
    name:string;
    description:string;
    createdAt:string;
    tickets: Ticket[];

    constructor(    
        id:number,
        name:string,
        description:string,
        createdAt:string,
        tickets: Ticket[],){
            this.id=id;
            this.name=name;
            this.description=description;
            this.createdAt=createdAt;
            this.tickets=tickets;
        }

}
