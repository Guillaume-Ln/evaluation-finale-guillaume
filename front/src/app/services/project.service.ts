import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Project } from '../model/project';
import { Observable, tap } from 'rxjs';
import { Ticket } from '../model/ticket';

@Injectable({
  providedIn: 'root'
})
export class ProjectService {
  projects : Project[]=[]
  tickets : Ticket[]=[]


  private projectsUrl ='/api/zira'; //Url to web api
  

  constructor(private http : HttpClient) { }

  getProjects(): Observable<Project[]>{
    return this.http.get<Project[]>(this.projectsUrl);
  }

  getProjectById(id: Number): Observable<Project> {
    return this.http.get<Project>(`${this.projectsUrl}/${id}`)
  }

  addProject(project:Project): Observable<Project> {
    return this.http
      .post<Project>(`${this.projectsUrl}`, project).pipe(
        tap((project) => {
          this.projects.push(project);
        })
      );
  
      //.pipe(catchError((error: any) => this.handleError(error)))
  }

  addTicket(ticket:Ticket, id:string): Observable<Ticket> {
    return this.http
      .post<Ticket>(`${this.projectsUrl}/${id}/tickets`, ticket).pipe(
        tap((ticket) => {
          this.tickets.push(ticket);
        })
      );
      //.pipe(catchError((error: any) => this.handleError(error)))
  }

  deleteProjectById(id: number): Observable<Project>{
    return this.http.delete<Project>(`${this.projectsUrl}/${id}`)
  }

}

