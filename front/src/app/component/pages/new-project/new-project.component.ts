import { Component} from '@angular/core';
import {FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Project } from 'src/app/model/project';
import { ProjectService } from 'src/app/services/project.service';


@Component({
  selector: 'app-new-project',
  templateUrl: './new-project.component.html',
  styleUrls: ['./new-project.component.scss']
})
export class NewProjectComponent{
  projects!: Project;
  constructor(private projectService: ProjectService, private router: Router) { }
  projectForm = new FormGroup({
    name: new FormControl('', [Validators.required]),
    description: new FormControl('', [Validators.required]),

  })

  async save() {    

    if (this.projectForm.valid) {
      
      const values = this.projectForm.value 
      const result = {...values} as unknown as Project
      this.projectService.addProject(result).subscribe()
      this.router.navigateByUrl('zira');
    }

  }



}