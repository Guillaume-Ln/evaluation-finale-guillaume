import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Route, Router } from '@angular/router';
import { Project } from 'src/app/model/project';
import { ProjectService } from 'src/app/services/project.service';

@Component({
  selector: 'app-projects-details',
  templateUrl: './projects-details.component.html',
  styleUrls: ['./projects-details.component.scss']
})
export class ProjectsDetailsComponent implements OnInit {

  project!: Project;

  constructor(private route:ActivatedRoute, private projectService : ProjectService, private router: Router){
    
  }

  ngOnInit(): void {
    this.loadProject();
  }

  private loadProject():void{
    const id = this.route.snapshot.params['id'] as number;
    this.projectService.getProjectById(id)
      .subscribe(project => this.project =project);
  }

  deleteProject(project:Project){
    this.projectService.deleteProjectById(project.id).subscribe(()=>{
      this.router.navigateByUrl(`/`)
    })
  }

}
