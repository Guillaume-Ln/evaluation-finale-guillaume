import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Project } from 'src/app/model/project';
import { Ticket } from 'src/app/model/ticket';
import { TicketStatus } from 'src/app/model/ticket-status';
import { ProjectService } from 'src/app/services/project.service';

@Component({
  selector: 'app-new-ticket',
  templateUrl: './new-ticket.component.html',
  styleUrls: ['./new-ticket.component.scss']
})
export class NewTicketComponent implements OnInit{
  id!: string
  project!: Project;

  tickets!: Ticket;
  keys = Object.keys;
  status = TicketStatus;
  constructor(private projectService: ProjectService, private router: Router, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.id = this.route.snapshot.paramMap.get('id') as string
    
  }
  ticketForm = new FormGroup({
    title: new FormControl('', [Validators.required]),
    content: new FormControl('', [Validators.required]),
    status: new FormControl ('',[Validators.required])

  })

  async save() {    

    if (this.ticketForm.valid) {
      
      const values = this.ticketForm.value 
      const result = {...values} as unknown as Ticket
      this.projectService.addTicket(result, this.id).subscribe()
      this.router.navigateByUrl('project/' + this.id);
    }

  }



}
