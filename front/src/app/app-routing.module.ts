import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProjectsComponent } from './component/pages/projects/projects.component';
import { ProjectsDetailsComponent } from './component/pages/projects-details/projects-details.component';
import { NewProjectComponent } from './component/pages/new-project/new-project.component';
import { NewTicketComponent } from './component/pages/new-ticket/new-ticket.component';


const routes: Routes = [

  {path: '', redirectTo:'zira', pathMatch:'full'},
  {path:'zira', component : ProjectsComponent},
  {path:'add', component: NewProjectComponent},
  {path:'project/:id', component : ProjectsDetailsComponent},
  {path:'project/:id/addTicket', component: NewTicketComponent},
  
 


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
